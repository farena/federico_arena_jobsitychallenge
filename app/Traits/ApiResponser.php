<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

trait ApiResponser
{
    protected function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }
    protected function errorResponse($message, $code)
    {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }
    protected function showAll(Collection $collection, $code = 200)
    {
        if ($collection->isEmpty() || count($collection) == 0) {
            return $this->successResponse(['data' => $collection], $code);
        }
        $collection = $this->filterData($collection);
        $collection = $this->searchData($collection);
        $collection = $this->sortData($collection);
        $collection = $this->paginate($collection);
        if (!env('APP_DEBUG')) $collection = $this->cacheResponse($collection);

        return $this->successResponse($collection, $code);
    }
    protected function showOne(Model $instance=null, $code = 200)
    {
        if (!$instance) $instance = [];
        return $this->successResponse(['data'=>$instance], $code);
    }
    protected function showMessage($message, $code = 200)
    {
        return $this->successResponse(['data' => $message], $code);
    }
    protected function filterData(Collection $collection)
    {
        foreach (request()->query() as $attribute => $value) {
            if (!in_array($attribute,['search','sortBy','sortByDesc','per_page','page','date_col'])) {

                if ($attribute == 'date_from') {
                    $date = explode('_',$value);
                    $collection = $collection->where(request()->date_col,'>',$date[2].'-'.$date[1].'-'.$date[0]);
                } elseif ($attribute == 'date_to') {
                    $date = explode('_',$value);
                    $collection = $collection->where(request()->date_col,'<',$date[2].'-'.$date[1].'-'.$date[0]);
                } else {
                    $collection = $collection->where($attribute,$value);
                }
            }
        }
        return $collection;
    }
    protected function searchData(Collection $collection)
    {
        foreach (request()->query() as $attribute => $value) {
            if ($attribute == 'search') {
                $keys= array_keys($collection[0]->getAttributes());

                $collection = $collection->filter(function ($item) use ($keys, $value) {
                    $valid = false;
                    foreach ($keys as $key) {
                        if(preg_match("/" . $value . "/i", $item->$key)) $valid = true;
                    }
                    return $valid;
                });

            }
        }
        return $collection;
    }
    protected function sortData(Collection $collection)
    {
        if (request()->has('sortBy')) {
            $collection = $collection->sortBy(request()->sortBy);
        }
        if (request()->has('sortByDesc')) {
            $collection = $collection->sortByDesc(request()->sortByDesc);
        }
        return $collection;
    }
    protected function paginate(Collection $collection)
    {
        $rules = [
                'per_page' => 'integer|min:2',
            ];

        Validator::validate(request()->all(), $rules);
        $page = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 15;
        if (request()->has('per_page')) {
            $perPage = (int) request()->per_page;
        }
        $results = $collection->slice(($page - 1) * $perPage, $perPage)->values();
        $paginated = new LengthAwarePaginator($results, $collection->count(), $perPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);
        $paginated->appends(request()->all());
        return $paginated;
    }

    protected function cacheResponse($data)
    {
        $url = request()->url();
        $queryParams = request()->query();

        ksort($queryParams);

        $queryString = http_build_query($queryParams);

        $fullUrl = $url."?".$queryString;

        return Cache::remember($fullUrl, 30/60, function() use($data) {
            return $data;
        });
    }

}
