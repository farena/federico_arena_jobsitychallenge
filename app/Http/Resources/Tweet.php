<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Tweet extends JsonResource
{

    public static function getTweetById($tweetId){
        $url = 'https://api.twitter.com/1.1/statuses/show/'.$tweetId.'.json';
        $requestMethod = 'GET';

        $twitter = new \TwitterAPIExchange([
            'oauth_access_token' => env('TW_TOKEN'),
            'oauth_access_token_secret' => env('TW_TOKEN_SECRET'),
            'consumer_key' => env('TW_CONSUMER_KEY'),
            'consumer_secret' => env('TW_CONSUMER_SECRET')
        ]);
        $content = $twitter->buildOauth($url, $requestMethod)
            ->performRequest();

        return json_decode($content);
    }
    public static function getTweetsByScreenName($screenName){
        $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
        $getfield = '?screen_name='.$screenName.'&tweet_mode=extended';
        $requestMethod = 'GET';

        $twitter = new \TwitterAPIExchange([
            'oauth_access_token' => env('TW_TOKEN'),
            'oauth_access_token_secret' => env('TW_TOKEN_SECRET'),
            'consumer_key' => env('TW_CONSUMER_KEY'),
            'consumer_secret' => env('TW_CONSUMER_SECRET')
        ]);
        $content = $twitter->setGetfield($getfield)
            ->buildOauth($url, $requestMethod)
            ->performRequest();

        return json_decode($content);
    }
}
