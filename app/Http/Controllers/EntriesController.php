<?php

namespace App\Http\Controllers;

use App\Http\Requests\EntryRequest;
use App\Models\Entry;
use App\Models\User;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class EntriesController extends ApiController
{
    /**
     * Display a list of entries for all users
     *
     * @return Response
     */
    public function index()
    {
        $entries = Entry::select('*')->orderBy('created_at','desc')->with('user')->get();

        return $this->showAll($entries);
    }

    /**
     * Store a newly created entry in storage.
     *
     * @param EntryRequest $request
     * @return Response
     */
    public function store(EntryRequest $request)
    {
        Entry::create([
            'title'=>$request->input('title'),
            'content'=>$request->input('content'),
            'user_id'=>Auth::user()->id,
        ]);

        return $this->showMessage('Entry created successfully!');
    }

    /**
     * Update the specified entry in storage.
     *
     * @param EntryRequest $request
     * @param Entry $entry
     * @return void
     * @throws AuthorizationException
     */
    public function update(EntryRequest $request, Entry $entry)
    {
        $this->authorize('modify', $entry);

        if ($entry->title != $request->input('title'))
            $entry->title = $request->input('title');

        if ($entry->content != $request->input('content'))
            $entry->content = $request->input('content');

        if (!$entry->isDirty())
            return $this->errorResponse('You must send something to update!',422);

        $entry->save();

        return $this->showMessage('Entry updated successfully!');
    }

    /**
     * Remove the specified entry from storage.
     *
     * @param Entry $entry
     * @return void
     * @throws Exception
     */
    public function destroy(Entry $entry)
    {
        $this->authorize('modify', $entry);

        $entry->delete();

        return $this->showMessage('Entry was deleted.');
    }
}
