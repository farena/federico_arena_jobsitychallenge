<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends ApiController
{
    public function login(LoginRequest $request){

        $user = User::where('email',$request->email)->first();


        if (!$user || !Hash::check($request->password, $user->password))
            return $this->errorResponse('Incorrect user or password.',403);


        try {
            $token = $user->createToken();
        } catch (\Exception $e) {
            return $this->errorResponse('Error in token creation, check if Passport installation is ok.',500);
        }

        return $this->showMessage([
            'user'=>$user,
            'token' => $token
        ]);
    }


    public function register(RegisterRequest $request){

        User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
            'twitter_username'=>$request->twitter_username
        ]);

        return $this->showMessage('User created successfully!');
    }

    public function show(User $user){
        return $this->showOne($user);
    }

}
