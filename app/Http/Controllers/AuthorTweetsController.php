<?php

namespace App\Http\Controllers;

use App\Http\Resources\Tweet;
use App\Models\HiddenTweets;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthorTweetsController extends ApiController
{
    public function index(User $user){

        $tweets = collect($user->tweets());
        $hiddenTweetsIds = HiddenTweets::where('user_id',$user->id)->get()->pluck('tweet_id');

        $tweets = $tweets->filter(function($x) use ($hiddenTweetsIds){
            foreach ($hiddenTweetsIds as $item) {
                if($item == $x->id_str){
                    return false;
                }
            }
            return true;
        });

        return $this->showAll($tweets);
    }

    public function loggedIndex(){

        $user = Auth::user();
        $hiddenTweetsIds = HiddenTweets::where('user_id',$user->id)->get()->pluck('tweet_id');

        $tweets = collect($user->tweets())->map(function($x) use ($hiddenTweetsIds){
            $x = collect($x);

            foreach ($hiddenTweetsIds as $item) {
                if($item == $x['id_str'])
                    $x['hidden']=true;
            }
            return $x;
        });

        return $this->showAll($tweets);
    }

    public function toggle(User $user, $tweetId){

        if (Auth::user()->id != $user->id)
            return $this->errorResponse('You are not authorized to access to this resource',403);


        $tweet = Tweet::getTweetById($tweetId);
        if(property_exists($tweet, "errors"))
            return $this->errorResponse('There is no tweets with that ID',404);

        if ($user->twitter_username != $tweet->user->screen_name)
            return $this->errorResponse("User doesn't match with this Tweet User",403);


        // Check if this Tweet is already hidden
        $isHidden = HiddenTweets::where('tweet_id',$tweetId)->first();

        // If is hidden, show it
        if ($isHidden) {
            $isHidden->delete();

            return $this->showMessage('Tweet unhidden');
        }


        // If not, hide it
        HiddenTweets::create([
            'user_id'=>$user->id,
            'tweet_id'=>$tweetId
        ]);

        return $this->showMessage('Tweet hidden');


    }
}
