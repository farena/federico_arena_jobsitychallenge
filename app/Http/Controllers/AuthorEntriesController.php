<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AuthorEntriesController extends ApiController
{
    public function index(User $user){
        return $this->showAll($user->entries);
    }
}
