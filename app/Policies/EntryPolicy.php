<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Entry;
use Illuminate\Auth\Access\HandlesAuthorization;

class EntryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the entry.
     *
     * @param User $user
     * @param Entry $entry
     * @return mixed
     */
    public function modify(User $user, Entry $entry)
    {
        return $user->id == $entry->user_id;
    }
}
