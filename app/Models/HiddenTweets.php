<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HiddenTweets extends Model
{
    protected $fillable = ['tweet_id','user_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
