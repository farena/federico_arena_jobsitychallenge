<?php

namespace App\Models;

use App\Http\Resources\Tweet;
use App\Traits\ApiTokensModifier;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable,ApiTokensModifier;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','twitter_username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function entries(){
        return $this->hasMany(Entry::class);
    }
    public function tweets(){
        return Tweet::getTweetsByScreenName($this->twitter_username);
    }
    public function hiddenTweets(){
        return $this->hasMany(HiddenTweets::class);
    }
}
