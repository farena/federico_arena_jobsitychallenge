<?php

use Illuminate\Database\Seeder;

class OauthClientsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('oauth_clients')->truncate();

        \DB::table('oauth_clients')->insert(array (
            0 =>
            array (
                'id' => 1,
                'user_id' => NULL,
                'name' => 'main-app',
                'secret' => 'LuJHaHCgzxlbe6Vr7Q8P6Jc2c4eSegp1qnzPShdX',
                'redirect' => 'http://localhost',
                'personal_access_client' => 1,
                'password_client' => 0,
                'revoked' => 0,
                'created_at' => '2019-09-07 02:55:17',
                'updated_at' => '2019-09-07 02:55:17',
            ),
        ));


    }
}
