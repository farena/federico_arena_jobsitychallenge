<?php

use App\Models\Entry;
use App\Models\User;
use Illuminate\Database\Seeder;

class EntriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('entries')->truncate();
        $users = User::all();
        foreach ($users as $user) {
            $user->entries()->createMany(factory(Entry::class,15)->make()->toArray());
        }
    }
}
