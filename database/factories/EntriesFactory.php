<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Entry;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Entry::class, function (Faker $faker) {
    return [
        'title' => $faker->realText($maxNbChars = 80, $indexSize = 2),
        'content' => $faker->realText($maxNbChars = 180, $indexSize = 2)
    ];
});
