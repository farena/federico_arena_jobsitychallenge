<?php

namespace Tests\Feature;

use App\Models\Entry;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthorEntriesModuleTest extends TestCase
{
    /** @test */
    public function can_retrieve_a_list_of_paginated_entries_for_user()
    {
        $user = $this->create('User');
        $entries = $user->entries()
            ->createMany(
                factory(Entry::class,5)->make()->toArray()
            );

        $response = $this->json('get','/api/author/'.$user->id.'/entries');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data',
            'current_page',
            'first_page_url',
            'from',
            'last_page',
            'last_page_url',
            'next_page_url',
            'path',
            'per_page',
            'prev_page_url',
            'to',
            'total',
        ]);
    }


}
