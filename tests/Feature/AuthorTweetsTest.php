<?php

namespace Tests\Feature;

use App\Models\HiddenTweets;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthorTweetsTest extends TestCase
{
    /** @test */
    public function can_retrieve_a_list_of_paginated_tweets_for_user()
    {
        $user = $this->create('User');

        $response = $this->json('get','/api/author/'.$user->id.'/tweets');


        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data',
            'current_page',
            'first_page_url',
            'from',
            'last_page',
            'last_page_url',
            'next_page_url',
            'path',
            'per_page',
            'prev_page_url',
            'to',
            'total',
        ]);
    }

    /** @test */
    public function can_retrieve_a_list_of_paginated_tweets_for_authenticated_user()
    {
        $user = $this->authenticate();

        $response = $this->json('get','/api/author/tweets');


        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data',
            'current_page',
            'first_page_url',
            'from',
            'last_page',
            'last_page_url',
            'next_page_url',
            'path',
            'per_page',
            'prev_page_url',
            'to',
            'total',
        ]);
    }

    /** @test */
    public function can_toggle_a_tweet_visibility_of_my_ownership()
    {
        $user = $this->authenticate();
        $tweet = $user->tweets()[0];

        $isHidden = HiddenTweets::where('tweet_id',$tweet->id_str)->first();


        $response = $this->json('put',"/api/author/$user->id/tweets/$tweet->id_str/toggle");


        $response->assertStatus(200);
        if ($isHidden){
            $response->assertExactJson([
                'data'=>'Tweet unhidden'
            ]);
        } else {
            $response->assertExactJson([
                'data'=>'Tweet hidden'
            ]);
        }
    }

    /** @test */
    public function cant_toggle_a_tweet_visibility_without_ownership()
    {
        $this->authenticate();
        $user = $this->create('User');


        $response = $this->json('put',"/api/author/$user->id/tweets/-1/toggle");

        $response->assertStatus(403);
        $response->assertExactJson([
            'code'=>403,
            'error'=>'You are not authorized to access to this resource'
        ]);
    }

    /** @test */
    public function cant_toggle_a_tweet_visibility_if_it_doesnt_exist()
    {
        $user = $this->authenticate();


        $response = $this->json('put',"/api/author/$user->id/tweets/-1/toggle");

        $response->assertStatus(404);
        $response->assertExactJson([
            'code'=>404,
            'error'=>'There is no tweets with that ID'
        ]);
    }
}
