<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersModuleTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('passport:install');
    }

    /** @test */
    public function user_can_see_index_view()
    {
        $response = $this->get('/');
        $response->assertSuccessful();
        $response->assertViewIs('index');
    }

    /** @test */
    public function user_can_login_with_correct_credentials()
    {
        $user = $this->create('User',[
            'password' => Hash::make('password'),
        ]);

        $response = $this->json('post','/api/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data'=>[
                'user'=>[
                    'id',
                    'name',
                    'email',
                    'twitter_username',
                    'updated_at',
                    'created_at',
                ],
                'token'
            ]
        ]);
    }

    /** @test */
    public function user_cant_login_with_incorrect_credentials()
    {
        $user = $this->create('User',[
            'password' => bcrypt('password'),
        ]);

        $response = $this->json('post','/api/login', [
            'email' => $user->email,
            'password' => 'invalid-password',
        ]);

        $response->assertStatus(403);
        $response->assertExactJson([
            'code'=>403,
            'error'=>'Incorrect user or password.',
        ]);
    }

    /** @test */
    public function can_register_new_user()
    {
        $user = $this->make('User',[
            'password' => $storedPw = bcrypt('password'),
        ]);

        $response = $this->json('post','/api/register', [
            'name'=>$user->name,
            'email'=>$user->email,
            'password'=>'password',
            'password_confirmation'=>'password',
            'twitter_username'=>$user->twitter_username
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('users',[
            'name' => $user->name,
            'email' => $user->email,
            'twitter_username' => $user->twitter_username,
        ]);
        $response->assertExactJson([
            'data'=>'User created successfully!',
        ]);
    }

    /** @test */
    public function cant_register_new_user_without_pw_confirmation()
    {
        $user = $this->make('User',[
            'password' => bcrypt('password'),
        ]);

        $response = $this->json('post','/api/register', [
            'name'=>$user->name,
            'email'=>$user->email,
            'password'=>'password',
            'twitter_username'=>$user->twitter_username
        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'code'=>422,
            'error'=>["password"=>["The password confirmation does not match."]],
        ]);
    }

    /** @test */
    public function cant_register_new_user_without_name()
    {
        $user = $this->make('User',[
            'password' => bcrypt('password'),
        ]);

        $response = $this->json('post','/api/register', [
            'email'=>$user->email,
            'password'=>'password',
            'password_confirmation'=>'password',
            'twitter_username'=>$user->twitter_username
        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'code'=>422,
            'error'=>["name"=>["The name field is required."]],
        ]);
    }

    /** @test */
    public function cant_register_new_user_without_email()
    {
        $user = $this->make('User',[
            'password' => bcrypt('password'),
        ]);

        $response = $this->json('post','/api/register', [
            'name'=>$user->name,
            'password'=>'password',
            'password_confirmation'=>'password',
            'twitter_username'=>$user->twitter_username
        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'code'=>422,
            'error'=>["email"=>["The email field is required."]],
        ]);
    }

    /** @test */
    public function cant_register_new_user_without_twitter_username()
    {
        $user = $this->make('User',[
            'password' => bcrypt('password'),
        ]);

        $response = $this->json('post','/api/register', [
            'name'=>$user->name,
            'email'=>$user->email,
            'password'=>'password',
            'password_confirmation'=>'password',
        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'code'=>422,
            'error'=>["twitter_username"=>["The twitter username field is required."]],
        ]);
    }

    /** @test */
    public function can_show_user()
    {
        $user = $this->create('User',[
            'password' => bcrypt('password'),
        ]);

        $response = $this->json('get','/api/author/'.$user->id);

        $response->assertStatus(200);
        $response->assertExactJson([
            'data'=>[
                "created_at" => (string)$user->created_at,
                "email" => $user->email,
                "id" => $user->id,
                "name" => $user->name,
                "twitter_username" => $user->twitter_username,
                "updated_at" => (string)$user->updated_at,
            ],
        ]);
    }

    /** @test */
    public function cant_show_user()
    {
        $user = $this->create('User',[
            'password' => bcrypt('password'),
        ]);

        $response = $this->json('get','/api/author/-1');

        $response->assertStatus(404);
    }

}
