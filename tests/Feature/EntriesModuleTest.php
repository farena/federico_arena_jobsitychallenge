<?php

namespace Tests\Feature;

use App\Models\Entry;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EntriesModuleTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('passport:install');
    }

    /** @test */
    public function can_retrieve_a_list_of_paginated_entries() {

        $user = $this->create('User')
            ->entries()
            ->createMany(
                factory(Entry::class,5)->make()->toArray()
            );

        $response = $this->json('get','/api/entries');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data',
            'current_page',
            'first_page_url',
            'from',
            'last_page',
            'last_page_url',
            'next_page_url',
            'path',
            'per_page',
            'prev_page_url',
            'to',
            'total',
        ]);
    }

    /** @test */
    public function can_store_an_entry() {

        $this->authenticate();

        $entry = $this->make('Entry')->toArray();


        $response = $this->json('post','/api/entries',$entry);

        $response->assertStatus(200);
        $response->assertExactJson([
            'data'=>'Entry created successfully!'
        ]);

        $this->assertDatabaseHas('entries',$entry);
    }

    /** @test */
    public function can_update_an_entry() {

        $user = $this->authenticate();

        $entry = $user->entries()->save($this->make('Entry'));

        $entry->title = 'Updated Title';
        $entry->content = 'Updated content';

        $response = $this->json('put',"/api/entries/$entry->id",$entry->toArray());

        $response->assertStatus(200);
        $response->assertExactJson([
            'data'=>'Entry updated successfully!'
        ]);

        $this->assertDatabaseHas('entries',$entry->toArray());
    }

    /** @test */
    public function can_delete_an_entry() {

        $user = $this->authenticate();

        $entry = $user->entries()->save($this->make('Entry'));

        $response = $this->json('delete',"/api/entries/$entry->id");

        $response->assertStatus(200);
        $response->assertExactJson([
            'data'=>'Entry was deleted.'
        ]);

        $this->assertDatabaseMissing('entries',['id'=>$entry->id]);
    }

    /** @test */
    public function cant_store_an_entry_without_title() {

        $this->authenticate();

        $entry = $this->make('Entry');


        $response = $this->json('post','/api/entries',[
            'content'=>$entry->content
        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'code'=>422,
            'error'=>['title'=>['The title field is required.']]
        ]);
    }

    /** @test */
    public function cant_store_an_entry_without_content() {

        $this->authenticate();

        $entry = $this->make('Entry');


        $response = $this->json('post','/api/entries',[
            'title'=>$entry->title
        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'code'=>422,
            'error'=>['content'=>['The content field is required.']]
        ]);
    }

    /** @test */
    public function cant_update_an_entry_because_is_not_mine() {

        $this->authenticate();
        $user = $this->create('User');

        $entry = $user->entries()->save($this->make('Entry'));

        $entry->title = 'Updated Title';
        $entry->content = 'Updated content';

        $response = $this->json('put',"/api/entries/$entry->id",$entry->toArray());

        $response->assertStatus(403);
        $response->assertExactJson([
            'code'=>403,
            'error'=>"You are not authorized to access this resource."
        ]);
    }

    /** @test */
    public function cant_delete_an_entry_because_is_not_mine() {

        $this->authenticate();
        $user = $this->create('User');

        $entry = $user->entries()->save($this->make('Entry'));

        $response = $this->json('delete',"/api/entries/$entry->id");

        $response->assertStatus(403);
        $response->assertExactJson([
            'code'=>403,
            'error'=>"You are not authorized to access this resource."
        ]);
    }

    /** @test */
    public function cant_access_without_authenticate() {

        $store = $this->json('post','/api/entries');
        $store->assertStatus(401);

        $update = $this->json('put','/api/entries/1');
        $update->assertStatus(401);

        $delete = $this->json('delete','/api/entries/1');
        $delete->assertStatus(401);
    }


}
