<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    public function create(string $model, array $atributes = []){
        return factory("App\\Models\\$model")->create();
    }
    public function make(string $model, array $atributes = []){
        return factory("App\\Models\\$model")->make();
    }

    public function authenticate(){
        Passport::actingAs(
            $user = $this->create('User'),
            []
        );

        return $user;
    }
}
