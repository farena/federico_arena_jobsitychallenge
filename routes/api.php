<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login','UserController@login');
Route::post('register','UserController@register');
Route::get('author/tweets','AuthorTweetsController@loggedIndex')->middleware('auth:api');
Route::get('author/{user}','UserController@show')->where('user','[0-9]+');
Route::get('author/{user}/entries','AuthorEntriesController@index');
Route::get('author/{user}/tweets','AuthorTweetsController@index');
Route::put('author/{user}/tweets/{tweetId}/toggle','AuthorTweetsController@toggle')->middleware('auth:api');
Route::resource('entries','EntriesController')->only(['index']);
Route::resource('entries','EntriesController')->only(['store','update','destroy'])->middleware('auth:api');
