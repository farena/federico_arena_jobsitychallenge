read -p "Are you installing in PRODUCTION? [Y/n]: " ENV
if [ -z ${ENV} ] || [ ${ENV} = "y" ] || [ ${ENV} = "Y" ]
then
  ENV='false'
else
  ENV='true'
fi

read -p "Enter DATABASE NAME: " DB_NAME
while [ -z ${DB_NAME} ]; do
  read -p "Enter DATABASE NAME: " DB_NAME
done

read -p "Enter DATABASE USERNAME: " DB_USER
while [ -z ${DB_USER} ]; do
  read -p "Enter DATABASE USERNAME: " DB_USER
done

read -p "Enter DATABASE PASSWORD: " DB_PW
while [ -z ${DB_PW} ]; do
  read -p "Enter DATABASE PASSWORD: " DB_PW
done

read -p "Enter DATABASE PORT | Default 3306: " DB_PORT
if [ -z ${DB_PORT} ]
then
  DB_PORT='3306'
fi

read -p "Enter DATABASE HOST | Default 127.0.0.1: " DB_HOST
if [ -z ${DB_HOST} ]
then
  DB_HOST='127.0.0.1'
fi

echo ""
echo "Now, we are setting up Twitter Oauth Plugin...."
echo "......"

read -p "Enter Twitter CONSUMER KEY: " TW_CKEY
while [ -z ${TW_CKEY} ]; do
  read -p "Enter Twitter CONSUMER KEY: " TW_CKEY
done

read -p "Enter Twitter CONSUMER SECRET: " TW_CSECRET
while [ -z ${TW_CSECRET} ]; do
  read -p "Enter Twitter CONSUMER SECRET: " TW_CSECRET
done

read -p "Enter Twitter TOKEN: " TW_TOKEN
while [ -z ${TW_TOKEN} ]; do
  read -p "Enter Twitter TOKEN: " TW_TOKEN
done

read -p "Enter Twitter TOKEN SECRET: " TW_TOKENSECRET
while [ -z ${TW_TOKENSECRET} ]; do
  read -p "Enter Twitter TOKEN SECRET: " TW_TOKENSECRET
done


sed s/%ENV%/${ENV}/ < .env.example > .env.temp && mv .env.temp .env.temp2
sed s/%DB_NAME%/${DB_NAME}/ < .env.temp2 > .env.temp && mv .env.temp .env.temp2
sed s/%DB_USER%/${DB_USER}/ < .env.temp2 > .env.temp && mv .env.temp .env.temp2
sed s/%DB_PW%/${DB_PW}/ < .env.temp2 > .env.temp && mv .env.temp .env.temp2
sed s/%DB_PORT%/${DB_PORT}/ < .env.temp2 > .env.temp && mv .env.temp .env.temp2
sed s/%DB_HOST%/${DB_HOST}/ < .env.temp2 > .env.temp && mv .env.temp .env.temp2
sed s/%TW_CKEY%/${TW_CKEY}/ < .env.temp2 > .env.temp && mv .env.temp .env.temp2
sed s/%TW_CSECRET%/${TW_CSECRET}/ < .env.temp2 > .env.temp && mv .env.temp .env.temp2
sed s/%TW_TOKEN%/${TW_TOKEN}/ < .env.temp2 > .env.temp && mv .env.temp .env.temp2
sed s/%TW_TOKENSECRET%/${TW_TOKENSECRET}/ < .env.temp2 > .env.temp && mv .env.temp .env

sed s/%TW_CKEY%/${TW_CKEY}/ < .env.testing.example > .env.temp && mv .env.temp .env.temp2
sed s/%TW_CSECRET%/${TW_CSECRET}/ < .env.temp2 > .env.temp && mv .env.temp .env.temp2
sed s/%TW_TOKEN%/${TW_TOKEN}/ < .env.temp2 > .env.temp && mv .env.temp .env.temp2
sed s/%TW_TOKENSECRET%/${TW_TOKENSECRET}/ < .env.temp2 > .env.temp && mv .env.temp .env.testing && rm .env.temp2

echo "Installing dependencies"
composer install
npm install

echo "Running Migrations & seeders"
php artisan migrate --seed

echo "Installing Passport & keys"
php artisan key:generate
php artisan passport:install

echo "Building VueJs Components"

if [ ${ENV} = "true" ]
then
  npm run dev
else
  npm run prod
fi

echo "Running Tests"
php artisan config:clear
./vendor/bin/phpunit
