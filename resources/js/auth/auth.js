export default function (Vue) {
    Vue.auth = {
        setToken(token,expires_at){

            localStorage.setItem('tokens',JSON.stringify({
                token:token,
                expires_at:expires_at
            }));

        },
        setIdentity(user){
            localStorage.setItem('identity',JSON.stringify(user));
        },
        getIdentity(){
            return JSON.parse(localStorage.getItem('identity'));
        },
        getToken(){
            var tokens = JSON.parse(localStorage.getItem('tokens'));
            if (tokens) {
                var token = tokens.token;
                var expires_at = tokens.expires_at;
            }

            if(!token || !expires_at) return null;
            if(Date.now() > parseInt(expires_at)){
                this.destroyToken();
                return  null;
            } else {
                return token;
            }
        },
        destroyToken(){
            localStorage.removeItem('tokens');
            localStorage.removeItem('identity');
        },
        isLoggedin(){
            return this.getToken() != null;
        },
    };
    Object.defineProperties(Vue.prototype,{
        $auth:{
            get:() =>{
                return Vue.auth;
            }
        }
    })
}
