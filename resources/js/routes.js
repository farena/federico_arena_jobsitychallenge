/**
 * Components Imports
 */
import NotFound from "./pages/NotFound";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Index from "./pages/Index";
import AuthorPage from "./pages/AuthorPage";



/**
 *  Routes settings
 */
export default [
    { path: '/', component: Index },
    { path: '/author/:author', component: AuthorPage },
    { path: '/register', component: Register },
    { path: '/login', component: Login },
    { path: '*', component: NotFound },
];
