export default {
    methods:{
        showError(err){
            console.log(err);
            if (typeof err.response.data.error === 'object') {
                for(let i in err.response.data.error){
                    for(let a in err.response.data.error[i]) {
                        this.$toast.error(err.response.data.error[i][a]);
                    }
                }
            } else {
                this.$toast.error(err.response.data.error);
            }
        }
    }
};
