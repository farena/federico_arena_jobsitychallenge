/**
 * Import Dependencies
 */
require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import Auth from './auth/auth';
import App from "./App";
import VueToast from 'vue-toast-notification'; //https://www.npmjs.com/package/vue-toast-notification
import 'vue-toast-notification/dist/index.css';
import errorHandler from "./mixins/errorHandler";
import './components/Paginable/paginable';



/**
 * Implement Plugins
 */
Vue.use(VueRouter);
Vue.use(Auth);
Vue.use(VueToast, { position: 'bottom-right' });


/**
 * Setup Routes
 * Add necessary headers
 */
const router = new VueRouter({mode:'history',routes:routes});

router.beforeEach((to, from, next) => {
    //Authorization
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + Vue.auth.getToken();
    next();
});


// ErrorHandler Mixin for every component.
Vue.mixin(errorHandler);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));




/**
 * Initialize VueJs App
 */
const app = new Vue(Vue.util.extend({ router }, App))
    .$mount('#app');
