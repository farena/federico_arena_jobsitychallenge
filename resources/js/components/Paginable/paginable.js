String.prototype.paginableUrl = function(params){

    let per_page = 3;
    let page = null;
    let search = null;
    let sortBy = null;
    let sortDir = null;
    if (params.per_page) per_page = params.per_page;
    if (params.page) page = params.page;
    if (params.search) search = params.search;
    if (params.sortBy) sortBy = params.sortBy;
    if (params.sortDir) sortDir = params.sortDir;

    let url = this;

    url += '?per_page='+per_page;

    if (page) url += '&page='+page;
    if (search) url += '&search='+search;
    if (sortDir) {
        if (sortDir === 'desc') {
            if (sortBy) url += '&sortBy='+sortBy;
        } else {
            if (sortBy) url += '&sortByDesc='+sortBy;
        }
    }

    return url;
};
