#Coding Challenge for Jobsity

### Author: Federico Arena

#### To Run the application
    
    cd /var/www/jobsitychallenge
    git clone https://farena@bitbucket.org/farena/federico_arena_jobsitychallenge.git federico_arena
    cd federico_arena
    sudo cp apache.conf /etc/apache2/sites-available/federico_arena.conf
    a2ensite federico_arena.conf
    systemctl restart apache2
    
    
    sudo nano /etc/hosts
    
    // append at the end of file
    127.0.0.1 federico-arena.jobsitychallenge.com
  
    // inside federico_arena folder
    bash install.sh
    
    
The install.sh Script will install all the necessary dependencies, then execute migrations, seeders, build the VueJs Components and Run Unit Tests.


Finally you must change these folders group to apache group and set permissions

    // Inside /var/www/jobsitychallenge/federico_arena
    sudo chown $(whoami):www-data -R storage bootstrap/cache vendor
    sudo chmod 775 -R storage bootstrap/cache vendor


####In case of Tests Error: "Caused by PDOException: could not find driver"

    sudo apt-get install php7.2-sqlite
    sudo systemctl restart apache2 


####In case of 404 Not Found Error:

    // Enable Apache2 mod rewrite
    sudo a2enmod rewrite
    sudo systemctl restart apache2 
